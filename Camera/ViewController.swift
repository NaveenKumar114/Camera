//
//  ViewController.swift
//  Camera
//
//  Created by Naveen Natrajan on 04/08/22.
//

import UIKit
class ViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var cameraContainerView: UIView!
    var imagePickers:UIImagePickerController?

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var insideView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addImagePickerToContainerView()
        insideView.layer.cornerRadius = insideView.frame.width / 2

        // Do any additional setup after loading the view.
    }

    func addImagePickerToContainerView(){

            imagePickers = UIImagePickerController()
        if UIImagePickerController.isCameraDeviceAvailable( UIImagePickerController.CameraDevice.front) {
                imagePickers?.delegate = self
            imagePickers?.sourceType = UIImagePickerController.SourceType.camera

                //add as a childviewcontroller
            addChild(imagePickers!)

                // Add the child's View as a subview
                self.cameraContainerView.addSubview((imagePickers?.view)!)
                imagePickers?.view.frame = cameraContainerView.bounds
                imagePickers?.allowsEditing = true
                imagePickers?.showsCameraControls = true
                imagePickers?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

            }
        }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        print(saveImage(image: pickedImage))
        
        let alert = UIAlertController(title: "Camera", message: "Image Saved Successfully", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { (_) in
            print("Saved")
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)

        
    }
    func saveImage(image: UIImage) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("test.png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
}

